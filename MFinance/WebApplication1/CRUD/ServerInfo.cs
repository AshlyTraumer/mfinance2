﻿public struct S_INFO
{
    public const string PATH = "mongodb://localhost:27017";
    public const string DB_NAME = "MFinDB";
    public const string USER_DB = "users";
    public const string INCOME_DB = "incomes";
    public const string COST_DB = "costs"; 
}
