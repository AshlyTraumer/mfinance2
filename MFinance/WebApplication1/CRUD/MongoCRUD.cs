﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.CRUD
{
    interface MongoCRUD<T>
    {
        List<T> GetList();
        T GetById(int id);
        void Create(T param);
        void Update(T param);
        void DeleteById(int id);
    }
}
