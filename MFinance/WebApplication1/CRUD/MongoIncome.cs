﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.CRUD
{
    public class MongoIncome:MongoCRUD<Income>
    {
        IMongoDatabase database;
        public MongoIncome()
        {
            MongoClient client = new MongoClient(S_INFO.PATH);
            database = client.GetDatabase(S_INFO.DB_NAME);
        }

        public List<Income> GetList()
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = new BsonDocument();
            return collection.Find(filter).ToList();
        }

        public Income GetById(int id)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = new BsonDocument("Id", new BsonDocument("$e", id));
            return collection.Find(filter).First();
        }

        public void Create(Income param)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            collection.InsertOne(param);
        }

        public void Update(Income param)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var result = collection.ReplaceOneAsync(new BsonDocument("Id", param.Id), param, new UpdateOptions { IsUpsert = true });

        }

        public void DeleteById(int id)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = Builders<Income>.Filter.Eq("Id", id);
            collection.DeleteOne(filter);
        }

    }
}