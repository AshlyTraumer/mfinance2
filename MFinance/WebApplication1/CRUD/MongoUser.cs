﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Configuration;
using WebApplication1.Models;
using WebApplication1.CRUD;

namespace WebApplication1.Rep
{
    public class MongoUser:MongoCRUD<User>
    {
        IMongoDatabase database;
        public MongoUser()
        {
             MongoClient client = new MongoClient(S_INFO.PATH);
             database = client.GetDatabase(S_INFO.DB_NAME);
           // string con = ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;
            //var client = new MongoClient(con);
        }

        public List<User> GetList()
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = new BsonDocument();
            return collection.Find(filter).ToList();
        }

        public User GetById(int id)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = new BsonDocument("Id", new BsonDocument("$e", id));
            return collection.Find(filter).First();
        }

        public void Create(User param)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            collection.InsertOne(param);
        }

        public void Update(User param)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var result = collection.ReplaceOneAsync(
                new BsonDocument("Id", param.Id), param, new UpdateOptions { IsUpsert = true });

        }

        public void DeleteById(int id)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = Builders<User>.Filter.Eq("Id", id);
            collection.DeleteOne(filter);
        }


    }
}