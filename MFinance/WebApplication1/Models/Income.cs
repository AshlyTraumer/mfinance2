﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Income
    {
        public ObjectId Id { get; set; }
        public ObjectId UserId { get; set; }
        public DateTime date { get; set; }
        public double Count { get; set; }
        public string Comment { get; set; }
    }
}