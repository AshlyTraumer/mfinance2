﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplication1.Rep;


namespace WebApplication1.Account
{
    public static class Account
    {
        private static IMongoDatabase database;

        public static void Create(string login,string password)
        {
            Rep.MongoUser mu = new Rep.MongoUser();
            File.Encrypt(password);
            mu.Create(new Models.User() { Login = login, Password = password });
        }
    }
}