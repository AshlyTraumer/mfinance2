﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.CRUD
{
    interface MongoCRUD<T>
    {
        List<T> GetList();
        T GetById(ObjectId id);
        int Create(T param);
        void Update(T param);
        void DeleteById(ObjectId id);
    }
}
