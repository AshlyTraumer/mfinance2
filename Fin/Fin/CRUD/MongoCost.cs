﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.CRUD
{
    public class MongoCost : MongoCRUD<Cost>
    {
        IMongoDatabase database;
        public MongoCost()
        {
            MongoClient client = new MongoClient(S_INFO.PATH);
            database = client.GetDatabase(S_INFO.DB_NAME);
        }

        public List<Cost> GetList()
        {
            var collection = database.GetCollection<Cost>(S_INFO.COST_DB);
            var filter = new BsonDocument();
            return collection.Find(filter).ToList();
        }

        public Cost GetById(ObjectId id)
        {
            var collection = database.GetCollection<Cost>(S_INFO.COST_DB);
            var filter = new BsonDocument("_id", new BsonDocument("$eq", id));
            return collection.Find(filter).First();
        }

        public int Create(Cost param)
        {
            var collection = database.GetCollection<Cost>(S_INFO.COST_DB);
            collection.InsertOne(param);
            return 0;
        }

        public void Update(Cost param)
        {
            var collection = database.GetCollection<Cost>(S_INFO.COST_DB);
            var result = collection.ReplaceOneAsync(new BsonDocument("_id", param.Id), param, new UpdateOptions { IsUpsert = true });

        }

        public void DeleteById(ObjectId id)
        {
            var collection = database.GetCollection<Cost>(S_INFO.COST_DB);
            var filter = Builders<Cost>.Filter.Eq("Id", id);
            collection.DeleteOne(filter);
        }

        public List<Cost> Select(ObjectId id)
        {
            var collection = database.GetCollection<Cost>(S_INFO.COST_DB);
            var filter = new BsonDocument("UserId", new BsonDocument("$eq", id));
            return collection.Find(filter).ToList();
        }
    }
}