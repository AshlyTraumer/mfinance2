﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.CRUD
{
    public class MongoIncome:MongoCRUD<Income>
    {
        IMongoDatabase database;
        public MongoIncome()
        {
            MongoClient client = new MongoClient(S_INFO.PATH);
            database = client.GetDatabase(S_INFO.DB_NAME);
        }

        public List<Income> GetList()
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = new BsonDocument();
            return collection.Find(filter).ToList();
        }

        public Income GetById(ObjectId id)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = new BsonDocument("_id", new BsonDocument("$eq", id));
            return collection.Find(filter).First();
        }

        public int Create(Income param)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            collection.InsertOne(param);
            return 0;
        }

        public void Update(Income param)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = Builders<Income>.Filter.Eq("_id", param.Id);
            collection.ReplaceOne(filter,param, new UpdateOptions { IsUpsert = true });
           // var result = collection.ReplaceOneAsync(new BsonDocument("_id", param.Id), param, new UpdateOptions { IsUpsert = true });

        }

        public void DeleteById(ObjectId id)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = Builders<Income>.Filter.Eq("Id", id);
            collection.DeleteOne(filter);
        }

        public List<Income> Select(ObjectId id)
        {
            var collection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = new BsonDocument("UserId", new BsonDocument("$eq", id));
            return collection.Find(filter).ToList();
        }

    }
}