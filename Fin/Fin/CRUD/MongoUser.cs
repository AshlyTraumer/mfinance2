﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Configuration;
using WebApplication1.Models;
using WebApplication1.CRUD;
using Models;
using System.Globalization;

namespace WebApplication1.Rep
{
    public class MongoUser:MongoCRUD<User>
    {
        IMongoDatabase database;
        private IMongoCollection<Income> IncCollection;
        private IMongoCollection<Cost> CosCollection;

        public MongoUser()
        {
             MongoClient client = new MongoClient(S_INFO.PATH);
             database = client.GetDatabase(S_INFO.DB_NAME);
           // string con = ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;
            //var client = new MongoClient(con);
        }

        public List<User> GetList()
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = new BsonDocument();
            return collection.Find(filter).ToList();
        }

        public User GetById(ObjectId id)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            
            var filter = new BsonDocument("_id", new BsonDocument("$eq", id));
            if (collection.Find(filter).Count() != 0)
            {
                return collection.Find(filter).First();
            }
            else
                return null;
        }

        public User GetByLogin(string login)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = new BsonDocument("Login", new BsonDocument("$eq", login));
            if (collection.Find(filter).Count() != 0)
            {
                return collection.Find(filter).First();
            }
            else
                return null;
        }

        public int Create(User param)
        {

            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            if (collection.Find(x => x.Login == param.Login).Count() == 0)
            {
                collection.InsertOne(param);
                return 0;
            }
            else return -1;

        }

        public int Update(User param)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = Builders<User>.Filter.Eq("_id", param.Id);
            if (collection.Find(x => x.Login == param.Login).Count() == 0)
            {
                collection.ReplaceOne(filter, param, new UpdateOptions { IsUpsert = true });
                return 0;
            }
            else return -1;
            

        }

        public void DeleteById(ObjectId id)
        {
            var collection = database.GetCollection<User>(S_INFO.USER_DB);
            var filter = Builders<User>.Filter.Eq("Id", id);
            collection.DeleteOne(filter);
        }

        public User getUserAndLists(string login, string from, string to)
        {
            User user = new MongoUser().GetByLogin(login);
            IncCollection = database.GetCollection<Income>(S_INFO.INCOME_DB);
            var filter = Builders<Income>.Filter.Eq("UserId", user.Id);
            user.Incomes = new List<Income>();
             List<Income> li= IncCollection.Find(filter).ToList();
            string[] fromList = from.Split('.');
            DateTime fl = new DateTime(Int32.Parse(fromList[2]), Int32.Parse(fromList[1]), Int32.Parse(fromList[0]));
            string[] toList = to.Split('.');
            DateTime tl = new DateTime(Int32.Parse(toList[2]), Int32.Parse(toList[1]), Int32.Parse(toList[0]));
            foreach (Income i in li)
            {
                string[] datelist = i.date.Split('.');
                DateTime dt = new DateTime(Int32.Parse(datelist[2]), Int32.Parse(datelist[1]), Int32.Parse(datelist[0]));
                if ((dt.CompareTo(fl) >= 0) && (dt.CompareTo(tl) <= 0))
                    user.Incomes.Add(i);

            }

            CosCollection = database.GetCollection<Cost>(S_INFO.COST_DB);
            var filt = Builders<Cost>.Filter.Eq("UserId", user.Id);
            user.Costs = new List<Cost>();
            List<Cost> lc=CosCollection.Find(filt).ToList();
            foreach (Cost i in lc)
            {
                string[] datelist = i.date.Split('.');
                DateTime dt = new DateTime(Int32.Parse(datelist[2]), Int32.Parse(datelist[1]), Int32.Parse(datelist[0]));
                if ((dt.CompareTo(fl) >= 0) && (dt.CompareTo(tl) <= 0))
                    user.Costs.Add(i);
            }

            return user;
        }

        void MongoCRUD<User>.Update(User param)
        {
            throw new NotImplementedException();
        }
    }
}