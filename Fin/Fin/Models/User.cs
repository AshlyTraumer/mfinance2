﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Models
{
    public class User
    {
        public ObjectId Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public double CurrentMoney { get; set; }
        public List<Income> Incomes { get; set; }
        public List<Cost> Costs { get; set; }
    }
}