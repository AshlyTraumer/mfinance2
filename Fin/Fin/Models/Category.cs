﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Category
    {
        List<string> IncomeInstance = new List<string>();
        List<string> CostInstance = new List<string>();
        public Category()
        {
            IncomeInstance.Add("Зарплата");
            IncomeInstance.Add("Стипендия");
            IncomeInstance.Add("Подарок");
            IncomeInstance.Add("Займ");

            CostInstance.Add("Продукты питания");
            CostInstance.Add("Одежда,обувь");
            CostInstance.Add("Хозяйственные товары");
            CostInstance.Add("Комунальные услуги");
            CostInstance.Add("Сотовая связь, интернет");
            CostInstance.Add("Автомобиль, проезд");
            CostInstance.Add("Дети");
            CostInstance.Add("Долг");
            CostInstance.Add("Отдых, развлечения");
            CostInstance.Add("Здоровье, красота");
            CostInstance.Add("Другое");
        }

        public List<string> getICategory()
        {
            return IncomeInstance;
        }

        public List<string> getCCategory()
        {
            return CostInstance;
        }


    }
}