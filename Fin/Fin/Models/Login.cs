﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public class Login
    {
        [Required(ErrorMessage = "* Введите имя (логин);")]
        public string Log { get; set; }

        [Required(ErrorMessage = "* Введите пароль;")]
        public string Password { get; set; }
    }
}