﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace Fin.Models
{
    public class PdfModel
    {
        public string Title { get; set; }
        public string All { get; set; }
        public List<LoggerModel> list { get; set; }
    }
}