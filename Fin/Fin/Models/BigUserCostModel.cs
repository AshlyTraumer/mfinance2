﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace Fin.Models
{
    public class BigUserCostModel
    {
        public User user { get; set; }
        public Cost cost { get; set; }
    }
}