﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class LoggerModel
    {
        public string Id { get; set; }
        public byte Table { get; set; }
        public string Count { get; set; }
        public string Category { get; set; }
        public string Date { get; set; }
        public string Comment { get; set; }
    }
}