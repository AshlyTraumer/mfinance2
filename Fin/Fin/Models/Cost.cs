﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Cost
    {
        public ObjectId Id { get; set; }
        public ObjectId UserId { get; set; }

        [Required(ErrorMessage = "* Введите дату в формате dd.mm.yyyy;")]
        [RegularExpression(@"[0-9]{2}.[0-9]{2}.[0-9]{4}", ErrorMessage = "Некорректная дата")]
        public string date { get; set; }

        [Required(ErrorMessage = "* Введите количество (руб)")]
        [Range(typeof(int), "0", "100000000")]
        public string Count { get; set; }

        public string Comment { get; set; }
        public string Category { get; set; }
    }
}