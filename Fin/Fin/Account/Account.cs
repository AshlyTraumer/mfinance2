﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplication1.Rep;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Text;
using Models;
using WebApplication1.Models;

namespace WebApplication1.Account
{
    public static class Account
    {
        private static IMongoDatabase database;

        public static int Create(Registr reg)
        {
            Rep.MongoUser mu = new Rep.MongoUser();
            if (mu.GetByLogin(reg.Login)== null)
            {                
                MD5 md5 = MD5.Create();
                if (mu.Create(new User()
                { Login = reg.Login, Password = GetMd5Hash(md5, reg.ConfirmPassword),CurrentMoney = 0 }) == 0)
                    return 0;
                else
                    return -1;
            }
            else
                return 1;
        }

        public static int Change(Registr reg)
        {
            Rep.MongoUser mu = new Rep.MongoUser();
            if (mu.GetByLogin(reg.Login) == null)
            {
                MD5 md5 = MD5.Create();
                if (mu.Update(new User()
                { Login = reg.Login, Password = GetMd5Hash(md5, reg.ConfirmPassword), CurrentMoney = 0 }) == 0)
                    return 0;
                else
                    return -1;
            }
            else
                return 1;
        }

        public static User Login(Login log)
        {
            Rep.MongoUser mu = new Rep.MongoUser();
            try {
                User user = mu.GetByLogin(log.Log);
                MD5 md5 = MD5.Create();
                if (user.Password.CompareTo(GetMd5Hash(md5, log.Password)) == 0)
                    return user;
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}