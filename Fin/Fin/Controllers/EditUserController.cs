﻿using Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Account;
using WebApplication1.Models;
using WebApplication1.Rep;

namespace Fin.Controllers
{
    public class EditUserController : Controller
    {
        // GET: EditUser
        public ActionResult Index(string login)
        {
            try {
                return View(new Registr() { Login = login });
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Commit(Registr model, string old)
        {
            if (ModelState.IsValid)
            {
                if (model.Login.Length < 3)
                {
                    ModelState.AddModelError("Login", "* Имя (логин) слишком короткое;");
                }
                else
                if (model.Password.Length < 5)
                {
                    ModelState.AddModelError("Password", "* Длина пароля не менее 5 символов;");
                }
                else
                {
                    
                    int status = Account.Change(model);

                    if (status != -1)
                    {
                        ObjectId id = new MongoUser().GetByLogin(model.Login).Id;
                        return RedirectToAction("Income", "Profile", new { user = id });
                    }
                    else
                        return View(model);
                }
            }
            return View(model);
        }
    }
}