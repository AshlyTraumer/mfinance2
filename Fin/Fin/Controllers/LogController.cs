﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.CRUD;
using WebApplication1.Models;
using WebApplication1.Rep;

namespace Fin.Controllers
{
    public class LogController : Controller
    {        
        public ActionResult Index(string user)
        {
            try {
                LoggerModel lm = new LoggerModel();
                ViewData["id"] = user;
                return View(new List<LoggerModel>());
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult GetLog(string from, string to, string login)
        {
            try { 
            List<LoggerModel> list = new List<LoggerModel>();
            //ViewData["id"] = user;
            MongoIncome mi = new MongoIncome();
            MongoCost mc = new MongoCost();
            MongoUser mu = new MongoUser();
            User user = mu.getUserAndLists(login, from, to);
                list = BuildLoggerModel(user);

            int a=0, b=0;
            foreach (LoggerModel l in list)
            {
                if (l.Table == 0)
                    a += Int32.Parse(l.Count);
                else
                    b += Int32.Parse(l.Count);
            }
            ViewData["login"] = user.Login;
            ViewData["from"] = from;
            ViewData["to"] = to;
            ViewData["income"] = a;
            ViewData["cost"] = b;
            list.Sort(CompareDate);

                MakePdf("Отчет с " + from + " по " + to+'\n', "Доход: " + a + " руб; расход: " + b + " руб; баланс: " + (a + b).ToString() + " руб \n", list);          
            return View(list);
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public FileResult Pdf(IEnumerable<LoggerModel> model)
        {
            Stream stream = new FileStream(Request.PhysicalApplicationPath + "result.pdf", FileMode.Open);
            return File(stream, "application/zip", "result.pdf");

        }

//-----------------------------------------------------------------------
        private List<LoggerModel> BuildLoggerModel(User user)
        {
            List<LoggerModel> list = new List<LoggerModel>();
            foreach (Income i in user.Incomes)
            {
                LoggerModel lm = new LoggerModel();
                lm.Category = i.Category;
                lm.Comment = i.Comment;
                lm.Count = '+' + i.Count;
                lm.Date = i.date;
                lm.Table = 0;
                list.Add(lm);
            }

            foreach (Cost i in user.Costs)
            {
                LoggerModel lm = new LoggerModel();
                lm.Category = i.Category;
                lm.Comment = i.Comment;
                lm.Count = '-' + i.Count;
                lm.Date = i.date;
                lm.Table = 1;
                list.Add(lm);
            }
            return list;
        }
        static public int CompareDate(LoggerModel lm1, LoggerModel lm2)
        {
            string[] Alm1 = lm1.Date.Split('.');
            string[] Alm2 = lm2.Date.Split('.');
            //DateTime dt = new DateTime();
            
            return new DateTime(Int32.Parse(Alm1[2]),Int32.Parse(Alm1[1]),Int32.Parse(Alm1[0]))
                .CompareTo(new DateTime(Int32.Parse(Alm2[2]), Int32.Parse(Alm2[1]), Int32.Parse(Alm2[0])));
        }
        private void MakePdf(string Title, string all, List<LoggerModel> list)
        {
            Document document = new Document();
            Category c = new Category();
            PdfWriter writer = PdfWriter.GetInstance(document,new FileStream(Request.PhysicalApplicationPath+"result.pdf", FileMode.Create));
            document.Open();
            string fg = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
            BaseFont fgBaseFont = BaseFont.CreateFont(fg, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            iTextSharp.text.Font fgFont = new iTextSharp.text.Font(fgBaseFont, 20, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            Phrase p = new Phrase(Title,fgFont);
            document.Add(p);

            fgFont = new iTextSharp.text.Font(fgBaseFont, 16, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            p = new Phrase(all, fgFont);
            document.Add(p);

            PdfPTable table = new PdfPTable(4);
            fgFont = new iTextSharp.text.Font(fgBaseFont, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            p = new Phrase("Количество", fgFont);
            PdfPCell  cell = new PdfPCell(p);
            cell.BorderWidth = 1;
            cell.Colspan = 1;
            cell.Rowspan = 1;
            table.AddCell(cell);

            p = new Phrase("Категория", fgFont);
            cell = new PdfPCell(p);
            cell.BorderWidth = 1;
            cell.Colspan = 1;
            cell.Rowspan = 1;
            table.AddCell(cell);

            p = new Phrase("Дата", fgFont);
            cell = new PdfPCell(p);
            cell.BorderWidth = 1;
            cell.Colspan = 1;
            cell.Rowspan = 1;
            table.AddCell(cell);

            p = new Phrase("Комментарий", fgFont);
            cell = new PdfPCell(p);
            cell.BorderWidth = 1;
            cell.Colspan = 1;
            cell.Rowspan = 1;
            table.AddCell(cell);

            foreach (LoggerModel lm in list)
            {
                p = new Phrase(lm.Count, fgFont);
                cell = new PdfPCell(p);
                cell.BorderWidth = 1;
                cell.Colspan = 1;
                cell.Rowspan = 1;
                table.AddCell(cell);

                if (lm.Table==0)
                    p = new Phrase(c.getICategory().ElementAt(Int32.Parse(lm.Category)), fgFont);
                else
                    p = new Phrase(c.getCCategory().ElementAt(Int32.Parse(lm.Category)), fgFont);
                cell = new PdfPCell(p);
                cell.BorderWidth = 1;
                cell.Colspan = 1;
                cell.Rowspan = 1;
                table.AddCell(cell);

                p = new Phrase(lm.Date, fgFont);
                cell = new PdfPCell(p);
                cell.BorderWidth = 1;
                cell.Colspan = 1;
                cell.Rowspan = 1;
                table.AddCell(cell);

                p = new Phrase(lm.Comment, fgFont);
                cell = new PdfPCell(p);
                cell.BorderWidth = 1;
                cell.Colspan = 1;
                cell.Rowspan = 1;
                table.AddCell(cell);
            }

            document.Add(table);
            document.Close();
            writer.Close();
        }        
    }
}

