﻿using Fin.Models;
using Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Account;
using WebApplication1.CRUD;
using WebApplication1.Models;
using WebApplication1.Rep;

namespace Fin.Controllers
{
    public class ProfileController : Controller
    {
        User u;
        public ActionResult Income(string user, string id)
        {
            try {
                MongoUser mu = new MongoUser();
                u = mu.GetById(new ObjectId(user));
                string s = Session["status"] as string;
                if (s == u.Login)
                {
                    u.Incomes = new MongoIncome().Select(u.Id);
                    BigUserIncomeModel big = new BigUserIncomeModel();
                    big.user = u;
                    big.user.Incomes = new MongoIncome().Select(new ObjectId(user));

                    big.income = new WebApplication1.Models.Income();




                    return View(big);

                }
                else
                    if (s == "null")
                    return RedirectToAction("Index", "Home");
                else
                {
                    u = mu.GetByLogin(s);
                    return RedirectToAction("Income", "Profile", new { user = u.Id });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Logon()
        {
            u = null;
            Session["status"] = "null";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Create(BigUserIncomeModel model, string categ)
        {
            if (ModelState.IsValid)
            {
                Income income = model.income;
                MongoUser mu = new MongoUser();
                income.UserId = mu.GetByLogin(model.user.Login).Id;
                MongoIncome mi = new MongoIncome();
                Category category = new Category();

                //income.Category=category.getICategory().IndexOf(income.Category).ToString();
                income.Category = category.getICategory().IndexOf(categ).ToString();
                mi.Create(income);
                model.user = mu.GetByLogin(model.user.Login);
                model.user.CurrentMoney += Int32.Parse(model.income.Count);
                mu.Update(model.user);
                return RedirectToAction("Income", "Profile", new { user = income.UserId });
            }
            
            return RedirectToAction("Income", "Profile", new { user = new MongoUser().GetByLogin(model.user.Login).Id });
        }

        
        public ActionResult Delete(string id, string userid)
        {
            MongoIncome mi = new MongoIncome();
            User user = new MongoUser().GetById(new ObjectId(userid));
            user.CurrentMoney -= Int32.Parse(mi.GetById(new ObjectId(id)).Count);
            mi.DeleteById(new ObjectId(id));
            new MongoUser().Update(user);
            return RedirectToAction("Income", "Profile", new { user = userid });
        }

        [HttpPost]
        public ActionResult Change(string id, string userid)
        {
             MongoIncome mi = new MongoIncome();
             MongoUser mu = new MongoUser();
             Income income = mi.GetById(new ObjectId(id));

             BigUserIncomeModel model = new BigUserIncomeModel();
             model.income = income;
             model.user = mu.GetById(income.UserId);
             model.user.Incomes = mi.Select(income.UserId);
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeItem(BigUserIncomeModel model, string categ, string id)
        {
            Category category = new Category();
            MongoIncome mi = new MongoIncome();
            MongoUser mu = new MongoUser();
            model.income.Category= category.getICategory().IndexOf(categ).ToString();
            model.income.UserId = new MongoUser().GetByLogin(model.user.Login).Id;
            model.income.Id = new ObjectId(id);
            model.user = mu.GetByLogin(model.user.Login);
            model.user.CurrentMoney -= Int32.Parse(mi.GetById(new ObjectId(id)).Count);
            model.user.CurrentMoney += Int32.Parse(model.income.Count);
            mu.Update(model.user);
            mi.Update(model.income);

           // return View(big);
            return RedirectToAction("Income", "Profile", new { user = model.income.UserId });
        }
    }
}