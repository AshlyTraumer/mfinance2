﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Account;
using WebApplication1.Models;

namespace Fin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            // WebSecurity.InitializeDatabaseConnection(S_INFO.PATH, "user", "_id", "login",false);
            // WebSecurity.CreateAccount("user","user");
            // WebSecurity.CreateUserAndAccount("user", "user");
            //Account.Create("user", "users");
            
            //string connectionString = ConfigurationManager.ConnectionStrings["mongodb"].ConnectionString;
            // var con = new MongoUrlBuilder(connectionString);

            //client = new MongoClient(connectionString);
            // database = client.GetDatabase(con.DatabaseName);
            // var collection = database.GetCollection<User>(S_INFO.USER_DB);

            return View();
        }

        [HttpPost]
        public ActionResult Index(Login login)
        {
            if (ModelState.IsValid)
            {
                    User user = Account.Login(login);
                
                if (user == null)
                {
                    ModelState.AddModelError("Log", "* Неверный логин или пароль;");
                    return View(login);
                }
                else
                {
                    Session["status"] = user.Login;

                    return RedirectToAction("Income", "Profile", new { user=user.Id});
                }
                
            }
            
            return View(login);
        }
    }
}