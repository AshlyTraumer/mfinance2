﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Account;

namespace Fin.Controllers
{
    public class RegistrController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Registr reg)
        {
            
            if (ModelState.IsValid)
            {
                if (reg.Login.Length < 3)
                {
                    ModelState.AddModelError("Login", "* Имя (логин) слишком короткое;");
                }
                else
                if (reg.Password.Length < 5)
                {
                    ModelState.AddModelError("Password", "* Длина пароля не менее 5 символов;");
                }
                else
                {
                    int status = Account.Change(reg);
                    if ( status == 0)
                        return RedirectToAction("Success");
                    else
                        if (status == 1)
                    {
                        ModelState.AddModelError("Login", "* Такой логин уже занят;");
                        return View(reg);
                    }
                    else
                        return RedirectToAction("Error");
                }
            }
            return View(reg);
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }


    }
}