﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fin.Models;
using Models;
using MongoDB.Bson;
using WebApplication1.CRUD;
using WebApplication1.Models;
using WebApplication1.Rep;

namespace Fin.Controllers
{
    public class GraphicController : Controller
    {
        // GET: Graphic
        public ActionResult Index(string user, string id)
        {
            User u;

            MongoUser mu = new MongoUser();
            try
            {
                u = mu.GetById(new ObjectId(user));
            }
            catch (Exception)
            {

                return RedirectToAction("Index", "Home");
            }


            string s = Session["status"] as string;

            if (s == u.Login)
            {
                u.Costs = new MongoCost().Select(u.Id);
                BigUserCIModel big = new BigUserCIModel();
                big.user = u;

                big.user.Costs = new MongoCost().Select(new ObjectId(user));
                big.user.Incomes = new MongoIncome().Select(new ObjectId(user));
                big.cost = new WebApplication1.Models.Cost();

                return View(big);
            }
            else
                if (s == "null")
                return RedirectToAction("Index", "Home");
            else
            {
                u = mu.GetByLogin(s);
                return RedirectToAction("Cost", "Cost", new { user = u.Id });
            }


            // ViewData["id"] = user;
            // return View();
        }
    }
}