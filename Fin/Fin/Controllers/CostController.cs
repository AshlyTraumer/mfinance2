﻿using Fin.Models;
using Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.CRUD;
using WebApplication1.Models;
using WebApplication1.Rep;

namespace Fin.Controllers
{
    public class CostController : Controller
    {
        User u;
        public ActionResult Cost(string user, string id)
        {
            try {
                MongoUser mu = new MongoUser();
                u = mu.GetById(new ObjectId(user));
                string s = Session["status"] as string;
                if (s == u.Login)
                {
                    u.Costs = new MongoCost().Select(u.Id);
                    BigUserCostModel big = new BigUserCostModel();
                    big.user = u;
                    big.user.Costs = new MongoCost().Select(new ObjectId(user));

                    big.cost = new WebApplication1.Models.Cost();





                    return View(big);
                }
                else
                    if (s == "null")
                    return RedirectToAction("Index", "Home");
                else
                {
                    u = mu.GetByLogin(s);
                    return RedirectToAction("Cost", "Cost", new { user = u.Id });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult Logon()
        {
            u = null;
            Session["status"] = "null";
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Create(BigUserCostModel model, string categ)
        {
            
            if (ModelState.IsValid)
            {
                Cost cost = model.cost;
                MongoUser mu = new MongoUser();
                cost.UserId = mu.GetByLogin(model.user.Login).Id;
                MongoCost mi = new MongoCost();
                Category category = new Category();

                //income.Category=category.getICategory().IndexOf(income.Category).ToString();
                cost.Category = category.getCCategory().IndexOf(categ).ToString();
                mi.Create(cost);
                model.user = mu.GetByLogin(model.user.Login);
                model.user.CurrentMoney -= Int32.Parse(model.cost.Count);
                mu.Update(model.user);
                return RedirectToAction("Cost", "Cost", new { user = cost.UserId });
            }

            return RedirectToAction("Cost", "Cost", new { user = new MongoUser().GetByLogin(model.user.Login).Id });
           
        }


        public ActionResult Delete(string id, string userid)
        {
            MongoCost mi = new MongoCost();
            User user = new MongoUser().GetById(new ObjectId(userid));
            user.CurrentMoney += Int32.Parse(mi.GetById(new ObjectId(id)).Count);
            mi.DeleteById(new ObjectId(id));
            new MongoUser().Update(user);
            return RedirectToAction("Cost", "Cost", new { user = userid });
        }

        [HttpPost]
        public ActionResult Change(string id, string userid)
        {
            
            MongoCost mi = new MongoCost();
            MongoUser mu = new MongoUser();
            Cost cost = mi.GetById(new ObjectId(id));
            BigUserCostModel model = new BigUserCostModel();
            model.cost = cost;
            model.user = mu.GetById(cost.UserId);
            model.user.Costs = mi.Select(cost.UserId);
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeItem(BigUserCostModel model, string categ, string id)
        {
            Category category = new Category();
            MongoCost mi = new MongoCost();
            MongoUser mu = new MongoUser();
            model.cost.Category = category.getCCategory().IndexOf(categ).ToString();
            model.cost.UserId = new MongoUser().GetByLogin(model.user.Login).Id;
            model.cost.Id = new ObjectId(id);
            model.user = mu.GetByLogin(model.user.Login);
            model.user.CurrentMoney += Int32.Parse(mi.GetById(new ObjectId(id)).Count);
            model.user.CurrentMoney -= Int32.Parse(model.cost.Count);
            mu.Update(model.user);
            mi.Update(model.cost);

            // return View(big);
            return RedirectToAction("Cost", "Cost", new { user = model.cost.UserId });
        }
    }
}